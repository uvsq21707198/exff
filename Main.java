/**
 * Created by m.najafi on 11/9/2017.
 */
import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    public static void main(String[] args){

        Fraction fraction = new Fraction(3, 4);
        Fraction f2 = new Fraction(3,9);
        System.out.println(fraction.value);
        System.out.println(fraction.add(f2));
        System.out.println(fraction.equals(f2));
        System.out.println(fraction.compareTo(new Fraction(3,2)));
        Fraction[] fractions = {
                new Fraction(1),
                new Fraction(10),
                new Fraction(9),
                new Fraction(4),
        };

        Arrays.sort(fractions);
        for (Fraction f :
                fractions) {
            System.out.println(f);
        }
    }
}
